import { ChalkInstance } from 'chalk';

export async function typeWriterEffect(text: string, speed: number, decorator: ChalkInstance) {
	const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

	for (const char of text) {
		process.stdout.write(decorator(char));
		await delay(speed);
	}
	console.log(); // Move to the next line after typing is complete
}
