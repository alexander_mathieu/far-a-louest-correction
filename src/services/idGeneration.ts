import { customAlphabet } from 'nanoid';
const nanoid = customAlphabet('1234567890ABDCEFGHIJKLMNOPQRSTUVWXYZ', 5);

export function generateId() {
	return nanoid();
}
