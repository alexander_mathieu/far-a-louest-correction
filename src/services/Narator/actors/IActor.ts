export interface CanSpeak {
	speak(text: string): Promise<void>;
}
