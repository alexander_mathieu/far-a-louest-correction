import { IsConnectable } from '../../services/db/IDb';
import { canCreatePlayer, canGetPlayer } from './IPlayer';
import { MongooseModel } from '../../services/db/MongooseConnector';

export class PlayerModelMongo implements canCreatePlayer, canGetPlayer {
	private db: MongooseModel;

	constructor(db: IsConnectable) {
		this.db = db.connect() as MongooseModel;
	}
	async getPlayer(name: string) {
		const player = await this.db.Player.findOne(
			{
				name: name,
			},
			{ _id: 1, name: 1 }
		).exec();

		if (!player) return null;
		return {
			name: player.name,
			id: player._id.toString(),
		};
	}
	async createPlayer(name: string) {
		const player = await this.db.Player.create({ name: name });

		return {
			name: player.name,
			id: player._id.toString(),
		};
	}
}
