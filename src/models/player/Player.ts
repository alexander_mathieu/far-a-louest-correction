import { IsConnectable } from '../../services/db/IDb';
import { canCreatePlayer, canGetPlayer } from './IPlayer';
import { PrismaClient } from '@prisma/client';

export class PlayerModel implements canCreatePlayer, canGetPlayer {
	private db: PrismaClient;

	constructor(db: IsConnectable) {
		this.db = db.connect() as PrismaClient;
	}
	async getPlayer(name: string) {
		return await this.db.player.findFirst({
			where: {
				name: name,
			},
		});
	}
	async createPlayer(name: string) {
		return await this.db.player.create({
			data: {
				name: name,
			},
		});
	}
}
